﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindMainCamera : MonoBehaviour {

    private GameObject mainCamera;

	void Start () {

        mainCamera = GameObject.Find("Main Camera");
        gameObject.GetComponent<Canvas>().worldCamera = mainCamera.GetComponent<Camera>();

	}
	
	void Update () {
		
	}
}
