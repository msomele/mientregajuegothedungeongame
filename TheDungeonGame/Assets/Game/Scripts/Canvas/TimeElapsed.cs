﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TimeElapsed : MonoBehaviour {

    private Text time;
    private PlayerManager player;
    private float timeElapsed;
    
    void Start () {

        time = GetComponent<Text>();
        player = FindObjectOfType<PlayerManager>();
        timeElapsed = PlayerPrefs.GetInt("CurrentPlayerTime");

	}
	
	void Update () {
		
        if (player.IsPlaying()) {

            timeElapsed += Time.deltaTime;
            time.text = FormatTime(timeElapsed);

        }
	}

    private string FormatTime(float value) {

        TimeSpan t = TimeSpan.FromSeconds(value);

        return string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);

    }

    public float GetTime() {
        return timeElapsed;
    }

    public string GetTimeAsText() {
        return time.text;
    }
}
