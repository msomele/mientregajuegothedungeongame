﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public string nextLevel;
    public GameObject loadingScreen;
    public GameObject HighScoreScreen;
    
    private AsyncOperation nextScene;
    private PlayerManager player;
    private bool loadLock;
    private string currentSceneName;

	void Start () {

        player = FindObjectOfType<PlayerManager>();
        currentSceneName = SceneManager.GetActiveScene().name;

	}
	
	// Update is called once per frame
	void Update () {
        
        if (!loadLock) { //if we are not loading a scene already

            if (Input.GetKeyDown("escape") && !SceneManager.GetActiveScene().name.Equals("0_Menu")) {

                nextLevel = "0_Menu";
                StartCoroutine(LoadNextLevel());

            } else if (player.finishedCurrentLvl) {

                StartCoroutine(LoadNextLevel());

            } else if (HighScoreScreen.activeSelf && Input.anyKeyDown  ) {

                HighScoreScreen.SetActive(false);

            }
        }

        
	}
    
    IEnumerator LoadNextLevel() {
        
        //Locks more than 1 loading at the same time and activates the loading screen
        loadLock = true;
        loadingScreen.SetActive(true);

        //Loads next scene async and doesn`t allow it to activate
        nextScene = SceneManager.LoadSceneAsync(nextLevel);
        nextScene.allowSceneActivation = false;

        while (!nextScene.isDone) {
            
            //Activates the text animation when scene is done
            if(nextScene.progress == 0.9f) {
                GameObject.Find("ContinueText").GetComponent<Text>().text = "Press any button to continue";
            }

            //Activates the scene when a button is pressed
            if (Input.anyKeyDown) {
                nextScene.allowSceneActivation = true;
            }

            yield return null;
        }
        
    }
    
    //Starts new game when the start button is pressed in Menu lvl
    public void StartNewGame() {

        ResetStats();
        player.finishedCurrentLvl = true;

    }

    //Resets the stats when new game is starting
    public void ResetStats() {
        
        PlayerPrefs.SetInt("PlayerCoins", 0);
        PlayerPrefs.SetInt("PlayerDeathCount", 0);
        PlayerPrefs.SetFloat("PlayerTime", 0);

        if (!PlayerPrefs.HasKey("BestTime")) {

            PlayerPrefs.SetInt("BestTime", 0);

        }

        if (!PlayerPrefs.HasKey("BestPlayer")) {

            PlayerPrefs.SetString("BestPlayer", "");

        }
    }

    //Used when High Score button is pressed in Menu
    public void HighScore() {

        HighScoreScreen.SetActive(true);

    }

    //Used when Exit button is pressed in Menu
    public void QuitGame() {

        Application.Quit();

    }

    private void PickNextLevelName() {

        //TBD
    }

    public string GetCurrentSceneName() {

        return currentSceneName;

    }
}
