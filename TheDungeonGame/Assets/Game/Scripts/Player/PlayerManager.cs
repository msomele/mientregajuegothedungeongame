﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public GameObject activeCheckpoint;
    public bool finishedCurrentLvl;
    public bool isDead;

    private bool isPlaying = true;
    private ScoreManager score;
    private GameManager game;

    private SoundManager sound;

    public bool hasKey = false;

    void Start() {

        score = FindObjectOfType<ScoreManager>();
        game = FindObjectOfType<GameManager>();
        sound = GetComponent<SoundManager>();

    }

    private void Update() {
        
        if (isDead && !sound.dyingSound.isPlaying) {

            Kill();

        }

    }

    public void Respawn() {

        isDead = false;
        var t = activeCheckpoint.transform.position;
        gameObject.transform.position = t;
        gameObject.SetActive(true);
        GetComponent<PlayerMovement>().enabled = true;

    }

    public void Kill() {
        
        gameObject.SetActive(false);
        score.AddDeath();
        Respawn();

    }

    public void FinishLevel() {

        finishedCurrentLvl = true;
        isPlaying = false;

        if (gameObject.name == "Player") {

            gameObject.SetActive(false);

        }

        SetPlayerScore();

        if (game.GetCurrentSceneName().Equals("Test")) {

            FinishGame();

        }

    }

    public void FinishGame() {

        score.HighScore();



        game.HighScoreScreen.SetActive(true);

    }
    
    //Old stats + new stats and setting them in memory
    private void SetPlayerScore() {

        int coins = PlayerPrefs.GetInt("PlayerCoins") + score.GetLevelCoins();
        int deaths = PlayerPrefs.GetInt("PlayerDeathCount") + score.GetLevelDeaths();
        float time = PlayerPrefs.GetFloat("PlayerTime") + score.GetLevelTime();

        PlayerPrefs.SetInt("PlayerCoins", coins);
        PlayerPrefs.SetInt("PlayerDeathCount", deaths);
        PlayerPrefs.SetFloat("PlayerTime", time);

    }

    public bool IsPlaying() {
        return isPlaying;
    }
}
