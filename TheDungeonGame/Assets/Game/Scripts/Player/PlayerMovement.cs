﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float maxMovementSpeed;
    public float maxJumpHight;

    public Transform groundCheck;
    public LayerMask ground;
    public float radius;

    private bool grounded;
    private Rigidbody2D body2D;
    private SpriteRenderer renderer2D;
    private SoundManager sound;

    public Animator Anim;
    public GameObject animator; 

    void Awake() {

        body2D = GetComponent<Rigidbody2D>();
        renderer2D = GetComponent<SpriteRenderer>();
        sound = GetComponent<SoundManager>();

    }

    void Start() {

        Anim = GetComponent<Animator>();

    }

    void FixedUpdate() {

        //Checks if there is ground beneath the player
        grounded = Physics2D.OverlapCircle(groundCheck.position, radius, ground);

        

    }

    void Update() {

        //Take the current velocity
        var velX = body2D.velocity.x;
        var velY = body2D.velocity.y;
        var absVelX = Mathf.Abs(velX);
        var absVelY = Mathf.Abs(velY);

        //Check for input
        var moving = Input.GetAxisRaw("Horizontal");
        bool jumping = Input.GetButtonDown("Jump");

        if (jumping) {

            Anim.SetInteger("State", -1);

        } else if (absVelX == 0 && absVelY == 0) {

            Anim.SetInteger("State", 0);

        } else if (absVelY == 0 && absVelX > 0) {

            Anim.SetInteger("State", 1);

        } 

        if (jumping && grounded) { //if we want to jump and we are not already in the air

            sound.JumpingSound();
            velY = maxJumpHight;

        }

        if (moving != 0) { //if we are pressing movement button  
            
            velX = maxMovementSpeed * moving; //applies the speed to the direction of the character
            renderer2D.flipX = moving < 0; //flips the character if we are moving left

        } else {

            velX = 0; //stops the character horizontal movement immediately

        }

        if (Mathf.Abs(velY) > 0) {

            velX *= 0.75f;

        }
         
        body2D.velocity = new Vector2(velX, velY);
        

    }
}
