﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioSource jumpingSound;
    public AudioSource dyingSound;
    private PlayerManager player;
    private PlayerMovement movement;
    private Rigidbody2D body;

	void Start () {

        player = GetComponent<PlayerManager>();
        movement = GetComponent<PlayerMovement>();
        body = GetComponent<Rigidbody2D>();

	}
	
	void Update () {
		
	}

    public void JumpingSound() {

        if (!jumpingSound.isPlaying) {

            jumpingSound.Play();

        }

    }

    public void PlayDyingSound() {
        
        dyingSound.Play();
        movement.enabled = false;
        body.velocity = new Vector2(0, body.velocity.y);
        player.isDead = true;

     }
    

    
}
