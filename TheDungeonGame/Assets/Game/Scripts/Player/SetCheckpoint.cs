﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCheckpoint : MonoBehaviour {

    private PlayerManager player;

    private void Start() {

        player = FindObjectOfType<PlayerManager>();

    }
    
    private void OnTriggerEnter2D(Collider2D target) {

        if (target.name == "Player") {

            player.activeCheckpoint = gameObject;

        }
    }
}
