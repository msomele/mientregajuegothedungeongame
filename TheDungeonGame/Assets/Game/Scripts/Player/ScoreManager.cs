﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
    
    private int deathCount;
    private int coins;    
    private TimeElapsed time;
        
    void Start () {
        
        time = FindObjectOfType<TimeElapsed>();
        deathCount = PlayerPrefs.GetInt("PlayerDeathCount");
        coins = PlayerPrefs.GetInt("PlayerCoins");       
        
    }    

    public void AddCoin() {
        coins++;
    }

    public void AddDeath() {
        deathCount++;
    }

    public float GetLevelTime() {
        return time.GetTime();
    }

    public int GetLevelDeaths() {
        return deathCount;
    }

    public int GetLevelCoins() {
        return coins;
    }

    public void HighScore() {

        var currentPlayerTime = PlayerPrefs.GetFloat("PlayerTime");
        var bestTime = PlayerPrefs.GetFloat("BestTime");

        if (currentPlayerTime < bestTime || bestTime == 0) {

            PlayerPrefs.SetFloat("BestTime", currentPlayerTime);
            PlayerPrefs.SetString("BestPlayer", "Time: " + time.GetTimeAsText() + "  Deaths: " + deathCount + "  Coins: " + coins);

        }

    }
}
