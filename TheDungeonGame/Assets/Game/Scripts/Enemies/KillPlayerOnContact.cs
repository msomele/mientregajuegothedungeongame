﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayerOnContact : MonoBehaviour {
    
    private SoundManager sound;

    private void Start() {
        
        sound = FindObjectOfType<SoundManager>();

    }

    void OnTriggerEnter2D(Collider2D other) {

        if (other.name == "Player") {

            if (!sound.dyingSound.isPlaying) {

                sound.PlayDyingSound();                

            }
            

        }

    }
}
