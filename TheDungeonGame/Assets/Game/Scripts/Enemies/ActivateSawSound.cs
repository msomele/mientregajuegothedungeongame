﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateSawSound : MonoBehaviour {

    private AudioSource sound;

	void Start () {

        sound = GetComponent<AudioSource>();

	}

    private void OnBecameVisible() {

        while(sound.volume != sound.maxDistance) {

            sound.volume += 1;

        }

    }

    private void OnBecameInvisible() {

        while (sound.volume != sound.minDistance) {

            sound.volume -= 1;

        }

    }
}
