﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLevel : MonoBehaviour {

    private PlayerManager player;

    private void Awake() {
        player = FindObjectOfType<PlayerManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {

        if (collision.name == "Player") {

            player.FinishLevel();            

        }
        
    }
}
