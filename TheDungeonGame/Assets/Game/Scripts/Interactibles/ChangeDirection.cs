﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeDirection : MonoBehaviour {

    public MovingPlatform platform;

    private void Start() {
        
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        
        if (collision.tag.Equals("MovingPlatform")) {

            platform.ChangeDirection();

        }            

    }
}
