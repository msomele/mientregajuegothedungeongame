﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectCoin : MonoBehaviour {

    private ScoreManager score;
    private Rigidbody2D body;
    private AudioSource sound;
    private bool isPickedUp;

	void Start () {

        score = FindObjectOfType<ScoreManager>();
        body = GetComponent<Rigidbody2D>();
        sound = GetComponent<AudioSource>();

	}

    private void OnTriggerEnter2D(Collider2D collision) {
        
        if ( collision.name.Equals("Player")) {

            isPickedUp = true;
            score.AddCoin();
            sound.Play();
            ExitAnim();

        }

    }

    private void ExitAnim() {

        var velX = 0;
        var velY = 200;

        body.velocity = new Vector2(velX, velY);

    }

    private void OnBecameInvisible() {

        if (isPickedUp) {

            Destroy(gameObject);

        }
        

    }

}
