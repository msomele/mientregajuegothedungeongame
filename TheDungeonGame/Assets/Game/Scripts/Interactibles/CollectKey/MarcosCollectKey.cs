﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarcosCollectKey : MonoBehaviour {


    public GameObject gameManager;

    public GameObject animatedObj;
    private Animator anim;
    public bool hasPlayed = false;
    public bool hasKey = false;


    private void OnTriggerEnter2D(Collider2D collision)//detecting a collision
    {

        if (collision.name.Equals("Player") && (!hasPlayed))// checks if collision made with player and if animation was played before
        {
            anim = animatedObj.GetComponent<Animator>(); // getting animation from GameObject
            anim.Play("Marcos_box_open");//playing the animation
            hasPlayed = true; // setting both properties to true, 
            hasKey = true;    // as the Player has the key and animation had already been played

            anim.SetBool("hasPlayed", true); // setting bool variable to true for the Animator

            gameManager = GameObject.Find("Player"); // getting Player GameObject
            gameManager.GetComponent<PlayerManager>().hasKey = true; // getting the access to PlayerManager variable and setting it to true

        }

    }
}
