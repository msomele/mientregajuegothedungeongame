﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorExitOpen : MonoBehaviour {

    public GameObject gameManager;
    public GameObject animatedObj;
    private Animator anim;
    private bool hasKey;

    private void OnTriggerEnter2D(Collider2D collision)//detecting a collision
    {
        gameManager = GameObject.Find("Player");// getting Player GameObject
        hasKey = gameManager.GetComponent<PlayerManager>().hasKey; // getting the access to PlayerManager variable 


        if (collision.name.Equals("Player") && hasKey)// checks if collision made with player and if animation was played before
        {
            anim = animatedObj.GetComponent<Animator>(); // getting animation from GameObject
            anim.Play("door_open");//playing the animation
            gameManager.GetComponent<PlayerManager>().finishedCurrentLvl = true;
        }

    }
}
