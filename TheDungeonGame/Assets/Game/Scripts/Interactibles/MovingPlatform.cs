﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {
    
    public bool active;
    public bool vertical;
    public bool goingUp;
    public bool goingRight;
    public int speed;   

	void FixedUpdate () {
		
        if (active) {

            if (vertical) {

                if (goingUp) {

                    transform.Translate(Vector2.up * speed);

                }
                else {

                    transform.Translate(Vector2.down * speed);

                }

            } else { //horizontal

                if (goingRight) {

                    transform.Translate(Vector2.right );

                } else {

                    transform.Translate(Vector2.left );

                }

            }

            

        }

	}

    private void OnCollisionEnter2D(Collision2D collision) {

        switch(collision.gameObject.name) {

            case "Player":
                active = true;
                collision.collider.transform.SetParent(transform);
                break;

            case "Top":
                goingUp = false;
                break;

            case "Bottom":
                goingUp = true;
                break;

            case "Right":
                goingRight = false;
                break;

            case "Left": 
                goingRight = true;
                break;
        }
    }

    void OnCollisionExit2D(Collision2D collision) {
        
        if (collision.gameObject.name.Equals("Player")) {

            collision.collider.transform.SetParent(null);

        }

    }

    public void ChangeDirection() {

        goingUp = !goingUp;
        goingRight = !goingRight;

    }

}
