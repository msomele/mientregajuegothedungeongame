﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayPlayerScore : MonoBehaviour {

    private TimeElapsed time;
    private PlayerManager player;

	void Start () {
        
        time = FindObjectOfType<TimeElapsed>();
        GetComponent<Text>().text = "Time: " + time.GetTimeAsText() +
                "  Deaths: " + PlayerPrefs.GetInt("PlayerDeathCount") + "  Coins: " + PlayerPrefs.GetInt("PlayerCoins");        
        
	}
	
	void Update () {
		
	}
}
