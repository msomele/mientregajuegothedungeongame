﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YourScore : MonoBehaviour {

    private PlayerManager player;

	void Start () {

        player = FindObjectOfType<PlayerManager>();

        if (player != null && !player.finishedCurrentLvl) {

            gameObject.SetActive(false);

        }

    }
	
	void Update () {
		
	}
}
